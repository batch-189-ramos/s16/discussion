let firstNumber = 12;
let secondNumber = 5;
let total = 0;

// ARITHMETIC OPERATORS

// Addition Operator - responsible for adding two or more numbers
total = firstNumber + secondNumber;
console.log('Result of addition operator is '+ total);

// Subtraction	 Operator - responsible for subtracting two or more numbers0- secondNumber;
total = firstNumber - secondNumber;
console.log('Result of subtraction operator is '+ total);

// Multiplication Operator - responsible for multiplying two or more numbers
total = firstNumber * secondNumber;
console.log('Result of multiplication operator is ' + total);

// Division Operator - responsible for dividing two or more numbers
total = firstNumber / secondNumber;
console.log('Result of division operator is ' + total);

// Modulo Operator - to show the remainder of the division; responsible for getting the remainder of 2 or more numbers
total = firstNumber % secondNumber;
console.log('Result of module operator is ' + total);

// ASSIGNMENT OPERATOR - 
// Reassignment Operator - should be a signle sign, signifies re-assignment or new value into existing variable
total = 27;
console.log('Result of reassignment operator is ' + total);

// Addition Assignment Operator - uses the current value of the variable and adds a number to itself. Afterwards, reassignes it as the new value of the variable.
// total = total + 3;
total += 3;
console.log('Result of addition assignment operator is ' + total);

// Subtraction Assignment Operator - uses the current value of the variable and subtracts a number to itself. Afterwards, reassignes it as the new value of the variable.
// total = total - 5;
total -= 5;
console.log('Result of subtraction assignment operator is ' + total);

// Multiplication Assignment Operator - uses the current value of the variable and multiplies a number to itself. Afterwards, reassignes it as the new value of the variable.
// total = total * 5;
total *= 4;
console.log('Result of multiplication assignment operator is ' + total);

// Division Assignment Operator - uses the current value of the variable and divides a number to itself. Afterwards, reassignes it as the new value of the variable.
// total = total / 20;
total /= 20;
console.log('Result of division assignment operator is ' + total);


// MULTIPLE OPERATORS
let mdasTotal = 0;
let pemdasTotal = 0;

/*
When doing multiple operations, the program follow the MDAS rule.
*/
mdasTotal = 2 + 1 - 5 * 4 /1;
console.log('Result of multiple operator is' + mdasTotal);

/*
When doing multiple operations, the program follow the PEMDAS rule.
*/

// Exponents are declared by double asterisks '**' after the number
pemdasTotal = 5**2 + (10 - 2) / 2 * 3;
console.log('Result of multiple operator is ' + pemdasTotal);


// INCREMENT AND DECREMENT OPERATORS
// 
let incrementNumber = 1;
let decrementNumber = 5;
/*
	Pre-Increment adds 1 first before reading the value
	Pre-Decrement dubtracts 1 first before reading the value

	Post-Increment reads value first before adding 1
	Post-Decrement reads value first before subtracting 1
*/
let resultOfPreIncrement = ++incrementNumber;
let resultOfPreDecrement = --decrementNumber;
let resultofPostIncrement = incrementNumber++;
let resultOfPostDecrement =decrementNumber--;

console.log(resultOfPreIncrement);
console.log(resultOfPreDecrement);
console.log(resultofPostIncrement);
console.log(resultOfPostDecrement);

// COERCION
// Coercion -When you add a string to a non-string value
let a ='10';
let b = 10;

console.log(a + b)


// Non-Coercion - When you 2 or more non-string values
let x = 10;
let y = 10;

console.log(x + y);


// Typeof Keyword - returns the data type of a variable
let stringType = 'hello';
let numberType = 1;
let booleanType = true;
let arrayType = ['1', '2', '3']
let objectType = {
	objectKey: 'Object Value'
};


console.log(typeof stringType);
console.log(typeof numberType);
console.log(typeof booleanType);
console.log(typeof arrayType);
console.log(typeof objectType);


// BOOLEAN 
// Computer reads 'true' as one
// Computer reads 'false' as zero
console.log(true + 1);
console.log(false + 1);

// COMPARISON OPERATORS
// Equality Operators - checks if both values are the same; returns true if it is
let number5 = 5;
console.log(number5 == 5);
console.log(5 == 5);
console.log('hello' == 'hello');
console.log(2 == '2');

// Strict Equality Operators - checks if both values and data types are the same, returns true if it is
console.log(2 === '2');
console.log(true === 1);

// Inequality Operator - checks if both values are not equal, returns true if they aren't
console.log(1 != 1);
console.log('hello' != 'hello');
console.log(2 != '2');

//Strict Inequality Operator - checks if both values and data types are not equal, returns true if they aren't
console.log(1 !== '1');


// RELATIONAL OPERATORS
let firstVariable = 5;
let secondVariable = 5;

// Greater than / GT Operator checks if first value is greater than the second value
console.log(firstVariable > secondVariable);
// Less than / LT Operator checks if the first vlaue is less than the second value
console.log(firstVariable < secondVariable);

// Greater than or Equal / GTE Operator checks if the first value is greater than or equal to the second value
console.log(firstVariable >= secondNumber);

// Less than or Equal / LTE Operator checks if the first value is less than or equal to the second value
console.log(firstVariable <= secondNumber);


//LOGICAL OPERATORS
let isLegalAge = true;
let isRegistered = false;

// AND Operator - checks if both values are true, returns true if it is
console.log(isLegalAge && isRegistered);

// OR Operator - checks if one of the values is true, returns true if it is and false if both values are false
console.log(isLegalAge || isRegistered);

// NOT Operator - Reverse the boolean value (from true to false; vice-versa)
console.log(!isLegalAge);



// Truthy and Falsey
// [], 0, '', false = falsey values
console.log([] == false)
console.log('' == false)
// console.log(null == false) null is a special value / empty value
console.log(0 == false)

// extra info.
let userLoggedIn = 'Rupert Ramos'
if(userLoggedIn)
// process content of the website or software




// Test
console.log(!isLegalAge || isRegistered)